


        let myForm = document.getElementById('my-form');
        myForm.addEventListener('focus', myFocusFunc, true);
        myForm.addEventListener('blur', myBlurFunction, true);



            function myBlurFunction() {
                let labelPrice = document.getElementById('label-price');
                let inputValue = document.getElementById('my-price').value;
                if (inputValue < 0 || isNaN(inputValue)) {
                    document.getElementById("my-price").style.border = "5px solid red";
                    let error = document.createElement('p');
                    error.innerHTML = `Please enter correct price`;
                    myForm.appendChild(error);
                } else {
                    let mySpan = document.createElement('span');
                    mySpan.innerHTML = `Текущая цена: ${inputValue}`;
                    myForm.insertBefore(mySpan, labelPrice);
                    let cancel = document.createElement('button');
                    cancel.innerHTML = ` X`;
                    mySpan.appendChild(cancel);
                    document.getElementById('my-price').style.backgroundColor = "green";
                }
            }

        function myFocusFunc() {
            document.getElementById("my-price").style.border = "5px solid green";
        }

        let cancel = document.getElementsByTagName('button');
        let mySpan = document.getElementsByTagName('span');
        cancel.onclick = function () {
            mySpan.remove();

        };








