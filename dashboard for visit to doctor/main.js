






    let modal = document.getElementById("modal");

    // --------------Get the button that opens the modal-----------------------

    let btnCreateModal = document.getElementById("btnCreate");
    let cancel = document.getElementsByClassName("cancel")[0];
    let caption = document.getElementById("caption");
    btnCreateModal.onclick = function() {
        modal.style.display = "block";
        caption.style.display = "none";
    };

    // ---------------When the user clicks on(x), close the modal---------------

    cancel.onclick = function() {
        modal.style.display = "none";
        caption.style.display = "block";
    };

   // When the user clicks anywhere outside of the modal, close it

  /*  document.onclick = function(event) {
        if (event.target.id !== 'modal' && modal.style.display !== 'none') {
            modal.style.display = "none";
        }
    };*/

    //-------------------Select the doctor------------------

    let choiceDentist = document.getElementById("selectOptions").options[1];
    let formDentist = document.getElementById("dentistForm");


    document.body.addEventListener('click',function (event) {
        if(event.target.selectedIndex > 0){
            if(document.querySelector('.showForm')){
                document.querySelector('.showForm').classList.remove('showForm');
                document.body.querySelectorAll('.visit-form')[event.target.selectedIndex-1].classList.add('showForm');
            }else{
                document.body.querySelectorAll('.visit-form')[event.target.selectedIndex-1].classList.add('showForm');

            }
            // formDentist.classList.add('showForm');
        }

    });

    choiceDentist.addEventListener('click', function () {
        // formDentist.style.display = "block";
        formDentist.classList.add('showForm');
    });

    let choiceTherapist = document.getElementById("selectOptions").options[2];
    let formTherapist = document.getElementById("therapistForm");
    choiceTherapist.addEventListener('click', function () {
        formTherapist.style.display = "block";
    });

    let choiceCardiologist = document.getElementById("selectOptions").options[3];
    let formCardiologist = document.getElementById("cardiologistForm");
    choiceCardiologist.addEventListener('click', function () {
        formCardiologist.style.display = "block";
    });

    //-----------Get the button that opens the visit-card-------------

    let btnOpenCard = document.getElementById("btnOpenCard");
    let selectOptions = document.getElementById("selectOptions");
    btnOpenCard.addEventListener('click', function getYourChoice() {

        switch (selectOptions.value) {
            case '1':  alert('Сделайте Ваш выбор');
                break;
            case '2': formDentist.style.display = "none";
                modal.style.display = "none";
                const dent = new VisitToDentist();
                dent.createCard();
                let storeCardDent = JSON.stringify(dent);
                localStorage.setItem("cardDent", storeCardDent);
                let returnStoreCardDent = JSON.parse(localStorage.getItem("cardDent"));
                break;
            case '3':   formTherapist.style.display = "none";
                modal.style.display = "none";
                const therapist = new VisitToTherapist();
                therapist.createCard();
                let storeCardTherapist = JSON.stringify(therapist);
                localStorage.setItem("cardTherapist", storeCardTherapist);
                // let returnStoreTherapist = JSON.parse(localStorage.getItem("cardTherapist"));
                break;
            case '4':  formCardiologist.style.display = "none";
                modal.style.display = "none";
                const cardiologist = new VisitToCardiologist();
                cardiologist.createCard();
                let storeCardCardiologist = JSON.stringify(cardiologist);
                localStorage.setItem("cardCardiologist", storeCardCardiologist);
                let returnStoreCardCardiologist = JSON.parse(localStorage.getItem("cardCardiologist"));
                break;
        }

    });

  // -------------- When the user clicks on(x), close the card--------------------

    document.addEventListener('click', function (event) {

    let cancelCard = document.getElementsByClassName("cancel");
    let cardDelete = document.getElementsByClassName('photo-doctor');

    for (let i = 1; i < cancelCard.length; i++) {
        if (event.target === cancelCard[1]) {
            localStorage.removeItem("cardDent");
            cardDelete[0].style.display = "none";
            // caption.style.display = "block";
        }
        if (event.target === cancelCard[2]) {
            localStorage.removeItem("cardTherapist");
            cardDelete[1].style.display = "none";
        }
        if (event.target === cancelCard[3]) {
            localStorage.removeItem("cardCardiologist");
            cardDelete[2].style.display = "none";
        }
    }
    });

    //---------------------Create card to visit----------------------

        class Visit {
            constructor(nameVisit, date, namePatient, photoDoctor, nameDoctor, comment, position) {
                this.nameVisit = {
                    description: "name visit",
                    value: nameVisit
                };
                this.date = {
                   description: "date",
                   value: date
                };
                this.namePatient = {
                    description: "name patient",
                    value: namePatient
                };
                this.photoDoctor = {
                    description: "photo doctor",
                    value: photoDoctor
                };
                this.nameDoctor = {
                    description: "name doctor",
                    value: nameDoctor
                };
                this.position = {
                    description: "position",
                    value: position
                };
                this.comment = {
                    description: "comment",
                    value: comment
                };
                this.info = [this.namePatient, this.photoDoctor, this.nameDoctor, this.position];
                this.dopInfo = [this.nameVisit, this.date, this.comment];
            }


            createCard() {
                let card = document.createElement('div');
                let mainContent = document.body.querySelector('#mainContent');
                mainContent.appendChild(card);
                console.log(mainContent);
                // document.body.getElementById('mainContent').appendChild(card);
                card.classList.add('photo-doctor');
                let namePatient = document.createElement('p');
                namePatient.classList.add('name-patient');
                card.appendChild(namePatient);
                namePatient.innerText=`${this.namePatient.description} ${this.namePatient.value}`;
                let cancel = document.createElement('img');
                cancel.src="images/Layer%205%20copy%203@1X.png";
                cancel.classList.add('cancel');
                namePatient.appendChild(cancel);
                let cardContainer = document.createElement('div');
                cardContainer.classList.add('card-container');
                namePatient.appendChild(cardContainer);
                let doctorPhoto = document.createElement('img');
                doctorPhoto.src = this.photoDoctor.value;
                cardContainer.appendChild(doctorPhoto);
                let  describeVisit = document.createElement('div');
                describeVisit.classList.add('describe-visit');
                cardContainer.appendChild(describeVisit);
                let nameDoctor = document.createElement('p');
                nameDoctor.classList.add('name-doctor');
                describeVisit.appendChild(nameDoctor);
                nameDoctor.innerText = `${this.nameDoctor.description} ${this.nameDoctor.value}`;
                let position = document.createElement('p');
                position.classList.add('position');
                position.innerText=`${this.position.description} ${this.position.value}`;
                describeVisit.appendChild(position);
                let more = document.createElement('p');
                more.classList.add('more');
                more.innerText='еще...';
                describeVisit.appendChild(more);
                let moreInfo = document.createElement('div');
                moreInfo.classList.add('more-information');
                moreInfo.style.display = "none";
                describeVisit.appendChild(moreInfo);

                for (let i = 0; i < this.dopInfo.length; i++) {
                    let p = document.createElement('p');
                    p.classList.add('more-info');
                    p.innerText=`${this.dopInfo[i].description} ${this.dopInfo[i].value}`;
                    moreInfo.appendChild(p);
                }

            }
        }




   class VisitToDentist extends Visit {
       constructor(name,date,purpose,comment,prevDate) {
           super(purpose, date, name, "images/141109581@1X.png", "Инна Торокина", comment, "Стоматолог");
           this.prevDate = {
               description: "Прошлый визит",
               value: prevDate
           };
           this.dopInfo.push(this.prevDate);
       }
   }


   class VisitToTherapist extends Visit {
            constructor(name,date, purpose,comment, age) {
                super(purpose, date, name, "images/male-doctor-png_63479@1X.png", "Бан Шихек", comment, "Терапевт")
                this.age = {
                    description: "age",
                    value: age
                };
                this.dopInfo.push(this.age);
            }
   }

    class VisitToCardiologist extends Visit {
        constructor(name,date, purpose,comment, age, bodyMass, normalPressure, pastIllnesses) {
            super(purpose, date, name, "images/Layer%204@1X.png", "Игорь Бондарь", comment, "Кардиолог");
            this.age = {
                description: "age",
                value: age
            };
            this.dopInfo.push(this.age);


            this.bodyMass = {
                description: "body mass",
                value: bodyMass
            };
            this.dopInfo.push(this.bodyMass);


            this.normalPressure = {
                description: "normal pressure",
                value: normalPressure
            };
            this.dopInfo.push(this.normalPressure);


            this.pastIllnesses = {
                description: "past illnesses",
                value: pastIllnesses
            };
            this.dopInfo.push(this.pastIllnesses);
        }


    }

      // // Make the DIV element draggagle
      //  dragElement(document.getElementById("cardVisitDentist"));
      //
      //  function dragElement(elem) {
      //      let pos1 = 0;
      //      let pos2 = 0;
      //      let pos3 = 0;
      //      let pos4 = 0;
      //      if (document.getElementById(elem.id + "Toper")) {
      //          document.getElementById(elem.id + "Toper").onmousedown = dragMouseDown;
      //      } else {
      //          elem.onmousedown = dragMouseDown;
      //      }
      //
      //      function dragMouseDown(e) {
      //          e = e || window.event;
      //          e.preventDefault();
      //          // get the mouse cursor position at startup:
      //          pos3 = e.clientX;
      //          pos4 = e.clientY;
      //          document.onmouseup = closeDragElement;
      //          // call a function whenever the cursor moves:
      //          document.onmousemove = elementDrag;
      //      }
      //
      //      function elementDrag(e) {
      //          e = e || window.event;
      //          e.preventDefault();
      //          // calculate the new cursor position:
      //          pos1 = pos3 - e.clientX;
      //          pos2 = pos4 - e.clientY;
      //          pos3 = e.clientX;
      //          pos4 = e.clientY;
      //          // set the element's new position:
      //          elem.style.top = (elem.offsetTop - pos2) + "px";
      //          elem.style.left = (elem.offsetLeft - pos1) + "px";
      //      }
      //
      //      function closeDragElement() {
      //          // stop moving when mouse button is released:
      //          document.onmouseup = null;
      //          document.onmousemove = null;
      //      }
      //
      //  }

    //--------------------- Save the data in local storage---------------------------

    function storeData () {
        if(localStorage.length){
            for (let i = 0; i < localStorage.length; i++) {
                if (localStorage["cardDent"]) {
                    const dent = new VisitToDentist();
                    dent.createCard();
                }
                if (localStorage["cardTherapist"]) {
                    const therapist = new VisitToTherapist();
                    therapist.createCard();
                }
                if (localStorage["cardCardiologist"]) {
                    const cardiologist = new VisitToCardiologist();
                    cardiologist.createCard();
                }
            }

        }

    }

    storeData();

