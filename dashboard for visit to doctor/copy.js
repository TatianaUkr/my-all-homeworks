
createCard() {
    let card = document.createElement('div');
    let mainContent = document.body.querySelector('#mainContent');
    mainContent.appendChild(card);
    console.log(mainContent);
    // document.body.getElementById('mainContent').appendChild(card);
    card.classList.add('photo-doctor');
    let namePatient = document.createElement('p');
    namePatient.classList.add('name-patient');
    card.appendChild(namePatient);
    namePatient.innerText=`${this.namePatient.description} ${this.namePatient.value}`
    let cancel = document.createElement('img');
    cancel.src="images/Layer%205%20copy%203@1X.png";
    cancel.classList.add('cancel');
    namePatient.appendChild(cancel);
    let cardContainer = document.createElement('div');
    cardContainer.classList.add('card-container');
    namePatient.appendChild(cardContainer);
    let doctorPhot = document.createElement('img');
    doctorPhot.src = this.photoDoctor.value;
    cardContainer.appendChild(doctorPhot);
    let  describeVisit = document.createElement('div');
    describeVisit.classList.add('describe-visit');
    cardContainer.appendChild(describeVisit);
    let nameDoctor = document.createElement('p');
    nameDoctor.classList.add('name-doctor');
    describeVisit.appendChild(nameDoctor);
    nameDoctor.innerText = `${this.nameDoctor.description} ${this.nameDoctor.value}`;
    let position = document.createElement('p');
    position.classList.add('position');
    position.innerText=`${this.position.description} ${this.position.value}`;
    describeVisit.appendChild(position);
    let more = document.createElement('p');
    more.classList.add('more');
    more.innerText='еще';
    describeVisit.appendChild(more);
    let moreInfo = document.createElement('div');
    moreInfo.classList.add('more-information');
    describeVisit.appendChild(moreInfo);

    for (let i = 0; i < this.dopInfo.length; i++) {
        let p = document.createElement('p');
        p.classList.add('more-info');
        p.innerText=`${this.dopInfo[i].description} ${this.dopInfo[i].value}`;
        moreInfo.appendChild(p);
    }

}
}




class VisitToDentist extends Visit {
    constructor(name,date,purpose,comment,prevDate) {
        super(purpose, date, name, "images/141109581@1X.png", "Инна Торокина", comment, "Стоматолог");
        this.prevDate={
            description: "Прошлый визит",
            value: prevDate
        };
        this.dopInfo.push(this.prevDate);
    }
}


class VisitToTherapist extends Visit {
    constructor(name,date, purpose,comment,age) {
        super(name, date, purpose, comment, "images/Layer%205%20copy%203@1X.png", "Бан Шихёк")
    }
}
const dent = new VisitToDentist('Olga Petrova','12/02/05','pain','none','15/05/04');
console.log(dent);
dent.createCard();