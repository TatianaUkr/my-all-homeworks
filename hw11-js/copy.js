

document.addEventListener('keydown', function(event) {
    let keys = [...document.getElementsByClassName('btn')];
    keys.forEach(elem => {
        elem.classList.remove('btn-colored');
        if (elem.innerText.toUpperCase() === event.key.toUpperCase()) {
            elem.classList.add('btn-colored')
        }
    })

});