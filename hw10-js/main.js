

const inputs = document.querySelectorAll('input');

document.addEventListener('click', function (event) {

    if (event.target.classList.contains('fas')) {
        [...inputs].forEach(el => {
            if (el.dataset.id === event.target.id) {
                event.target.classList.toggle('fa-eye-slash');
                if (el.type === 'password') {
                    el.type = 'text';
                } else {
                    el.type = 'password'
                }
            }
        })
    }
    if (event.target.id === 'btn') {
        const error = document.querySelector('.error');
        if ([...inputs][0].value === [...inputs][1].value) {
            error.style.display = "none";
            alert('You are welcome!')
        } else {
            error.style.display = "block"
        }
    }

});


